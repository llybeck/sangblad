#!/bin/bash

dir=build
file=sangblad
booklet=booklet

function run_command() {
	echo
	echo "=================================================================="
	echo "Running command: $@"
	echo "=================================================================="
	echo
	"$@"
}

function check_ok() {
	ret=$?
	if [ $ret -ne 0 ]; then
		exit
	fi
}

mkdir -vp $dir
run_command rm -fv $dir/*

run_command pdflatex --output-directory=$dir $file
check_ok

run_command biber $dir/$file
check_ok

run_command pdflatex --output-directory=$dir $file
check_ok

run_command pdflatex --output-directory=$dir $file
check_ok

run_command pdflatex --output-directory=$dir "\def\bookletfilename{${dir}/${file}.pdf} \input{$booklet}"
check_ok


